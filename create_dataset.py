#!/usr/bin/env python3

from recipe import Recipe
from threading import Thread
import json

THREADS = 3
data = []
recipe_urls = open("recipes.txt").read().splitlines()

def download(urls: [str]):
    global data
    for link in urls:
        data.append(Recipe(link).__dict__)
        print(f"Finished downloading {link}")

assignments = [[recipe_urls[j] for j in range(i,len(recipe_urls), THREADS)] for i in range(THREADS)]

threads = [Thread(target=download,args=(assignments[i],)) for i in range(THREADS)]

for t in threads:
    t.start()
for t in threads:
    t.join()

open("data.json", "w").write(json.dumps(data))
