#!/usr/bin/env python3

import bs4 as bs
import requests
import json
import re
from anyascii import anyascii

_get = lambda url: requests.get(url).text
_recipe = lambda url: bs.BeautifulSoup(_get(url), 'html.parser').find('script', {'type': 'application/ld+json'})

def remove_unicode(s:str):
    return str(anyascii(s))

class Recipe:

    def __init__(self, url):
        self.url = url

        data = json.loads(_recipe(url).contents[0])
        recipe = data
        # mutate where the recipe is depending on some hacked together rules.
        if type(data) == list:
            recipe = data[1] if "recipeIngredient" in data[1] else data[0]

        # set the name and the ingredients list.
        self.name = recipe["name"] if "name" in recipe else data["name"]
        self.ingredients = list(map(remove_unicode,recipe["recipeIngredient"]))

        # set the instructions and concatenate them into a single string.
        instructions = recipe["recipeInstructions"]
        for index in range(len(instructions)):
            instructions[index] = remove_unicode(instructions[index]["text"]) \
                    if "text" in instructions[index] else ""
        self.instructions = "".join(instructions)

    def __repr__(self):
        return f"Recipe(name='{self.name}',\n\tingredients={self.ingredients}, \n\tinstructions={self.instructions})"

    def json(self):
        return json.dumps(self.__dict__)

    def _test_ok(self):
        print(f"Ok, got data for {self.url}")

    @classmethod
    def _test_dbg(cls, url):
        data = json.loads(_recipe(url).contents[0])
        print(json.dumps(data))

def _tests():
    print("Testing the parser...")
    Recipe("https://www.foodandwine.com/recipes/rum-and-orange-cocktail")._test_ok()
    Recipe("https://www.bbcgoodfood.com/recipes/one-pan-easter-lamb")._test_ok()
    Recipe("https://www.thepioneerwoman.com/food-cooking/recipes/a39132657/lemon-ricotta-pancakes-recipe/")._test_ok()
    Recipe("https://www.foodnetwork.com/recipes/food-network-kitchen/skillet-chicken-thighs-with-white-winebutter-sauce-11585712")._test_ok()
    Recipe("https://www.allrecipes.com/recipe/23974/pasta-with-peas-and-sausage/")._test_ok()
    Recipe("https://www.simplyrecipes.com/scotch-eggs-recipe-5222866")._test_ok()
    Recipe("https://www.food.com/recipe/croque-monsieur-244200")._test_ok()
    Recipe("https://www.myrecipes.com/recipe/meyer-lemon-chicken")._test_ok()
    Recipe("https://www.epicurious.com/recipes/food/views/thenthuk-tibetan-pull-noodle-soup")._test_ok()
    Recipe("https://www.bonappetit.com/recipe/rigatoni-with-sausage-beans-and-greens")._test_ok()

def _dbg():
    url =\
    "https://www.allrecipes.com/recipe/214924/farro-salad-with-asparagus-and-parmesan/"
    print(_recipe(url).contents[0])
