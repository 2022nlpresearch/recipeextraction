# Recipe Extraction Toolkit

Python scripts for gathering recipe sample data for research purposes.

## Installation

1. You need a compatible version of python3 and poetry on your system.
2. Git clone this repository (and cd into it).
3. Run `poetry install`.

## Obtaining the data.json file

1. cd into the directory where the repository is stored.
2. Run `poetry shell` (This creates a shell with all the python dependencies
   already present)
3. Run `./create_dataset.py` - This will create a data.json file in the current
   directory based on the recipes in `recipes.txt` (These have been specifically
   selected from websites that have easy-to-parse recipes)
